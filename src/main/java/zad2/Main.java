package zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Zadanie zadanie = new Zadanie();

        Thread watek = new Thread(zadanie);
        watek.start();
        Scanner sc = new Scanner(System.in);
        String linia;

        do {

            linia = sc.nextLine();
            if (linia.equalsIgnoreCase("quit")){
                watek.interrupt();
            }
            zadanie.setTekst(linia);
        }while(!linia.equalsIgnoreCase("quit"));
    }
}
