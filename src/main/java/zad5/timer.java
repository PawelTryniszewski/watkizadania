package zad5;

public class timer implements Runnable {
    long time;

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(time);
            System.out.printf("twoj timer %d s - wake up! \n",time/1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
