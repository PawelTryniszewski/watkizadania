package zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Zadanie3 patrz = new Zadanie3();
        Thread watek = new Thread(patrz);
        watek.start();
        Scanner sc = new Scanner(System.in);
        String linia;
        do {
            linia = sc.nextLine();
            if (!linia.equalsIgnoreCase("quit")) {
                patrz.setPath(linia);
            }

            if (linia.equalsIgnoreCase("quit")) {
                watek.interrupt();
            }
        } while (!linia.equalsIgnoreCase("quit"));
    }
}

