package CostamDoLiczbPIerwszych;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        //System.out.println(czyPierwsza(7));
        BufferedReader in = new BufferedReader(new FileReader("numbers.txt"));
        String inputLine;
        long a = System.currentTimeMillis();
        int licznik = 0;

        while ((inputLine = in.readLine()) != null) {



            try {
                //String[] tab = inputLine.split(" ");
//                for (int i = 0; i < tab.length; i++) {
                //System.out.println(czyPierwsza(Long.valueOf(inputLine)));
                if (czyPierwsza(Integer.valueOf(inputLine))) {
                    licznik++;
                }
//                }
            } catch (NumberFormatException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
        in.close();
        long b = System.currentTimeMillis();
        System.out.println((b - a) / 1000 + " sekund");
        System.out.println(licznik);
    }

    public static boolean czyPierwsza(int i) {
        int podzielnik = 2;
        if (i > 3) {
            while (podzielnik < i) {
                if (i % podzielnik == 0) {
                    return false;
                }
                podzielnik++;
            }
        }
        return true;
    }
}
