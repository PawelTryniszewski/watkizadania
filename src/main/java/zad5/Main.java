package zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        timer timer = new timer();
        Timer1 timer1 = new Timer1();
        Timer2 timer2 = new Timer2();
        Timer3 timer3 = new Timer3();
        Thread watek = new Thread(timer);
        Thread watek1 = new Thread(timer1);
        Thread watek2 = new Thread(timer2);
        Thread watek3 = new Thread(timer3);
        Scanner sc = new Scanner(System.in);
        String a;
        int b = 0;
        do {
            a = sc.nextLine();
            if (a.equalsIgnoreCase("start")) {
                timer.setTime(sc.nextLong());
                watek.start();
                timer1.setTime(sc.nextLong());
                watek1.start();
                timer2.setTime(sc.nextLong());
                watek2.start();
                timer3.setTime(sc.nextLong());
                watek3.start();
            }if (a.equalsIgnoreCase("quit")){
                if (watek.isAlive()) {
                    b++;
                }if (watek1.isAlive()) {
                    b++;
                }if (watek2.isAlive()) {
                    b++;
                }if (watek3.isAlive()) {
                    b++;
                }System.out.printf("%d wątkow aktywnych\n", b);
            }
            Thread.sleep(1000);


        } while (!a.equalsIgnoreCase("quit"));
        System.out.println("wyszlo z petli i czeka na koniec");

        try {
            watek.join();
            watek1.join();
            watek2.join();
            watek3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Wszystkie watki sie skonczyly. Koniec programu.");

    }
}
