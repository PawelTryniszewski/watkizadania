package CalkowanieNumeryczne;

public class Main {
    private static double function(double x) {
        return (3*Math.sin(x)) - (0.2*Math.pow(x, 3)) + (3*Math.pow(x, 2));
    }

    public static void main(String[] args) {
        double xp, xk, dx, calka;
        double n;
        //Podaj poczatek przedzialu calkowania
        xp = 0.1;
        //Podaj koniec przedzialu calkowania
        xk = 15;
        //Podaj dokladnosc calkowania
        n = 1500;
        dx = (xk - xp) / n;
        calka = 0;
        long z = System.currentTimeMillis();
        for (int i=1; i<=n; i++) {
            calka += function(xp + i * dx);
        }
        calka *= dx;
        long x = System.currentTimeMillis();
        System.out.println(x-z+" milisekund");
        System.out.println("Wartość całki wynosi w przyblizeniu " + calka);
    }
}
