package JasiMalgosia;

import java.util.concurrent.CountDownLatch;

public class Jas implements Runnable {
    @Override
    public void run() {
        System.out.println("Jas przygotowuje jedzenie");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jas Skonczyl przygotowywac");
        System.out.println("Jas Idzie pod prysznic");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jas skonczyl prysznic");
        System.out.println("Jas idzie na zakupy");
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jas wrocil z zakupow");
        System.out.println("Jas gra na konsoli");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jas skonczyl");

        Main.countDownLatch.countDown();

    }
}
