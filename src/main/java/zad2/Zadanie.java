package zad2;

public class Zadanie implements Runnable {

    private String tekst;

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Hello world " + tekst);
                Thread.sleep(5000);
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
