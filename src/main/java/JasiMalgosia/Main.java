package JasiMalgosia;

import java.util.concurrent.CountDownLatch;

public class Main {
       static CountDownLatch countDownLatch = new CountDownLatch(2);
    public static void main(String[] args) throws InterruptedException {
        Jas jas = new Jas();
        Malgosia malgosia = new Malgosia();
        Thread jas1= new Thread(jas);
        Thread malgosia1 = new Thread(malgosia);
        jas1.start();
        malgosia1.start();
//        jas1.join();
//        malgosia1.join();
//        while(malgosia1.isAlive()||jas1.isAlive()){
//            Thread.sleep(700);
//        }
        countDownLatch.await();
        System.out.println("Koniec dnia");
    }
}
