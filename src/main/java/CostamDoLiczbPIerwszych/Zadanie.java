package CostamDoLiczbPIerwszych;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class Zadanie {
    private static AtomicInteger licznik = new AtomicInteger();
    private static CountDownLatch latch = new CountDownLatch(5000);

    public static void main(String[] args) throws IOException, InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(4);
        BufferedReader in = new BufferedReader(new FileReader("numbers.txt"));
        String inputLine;
        long a = System.currentTimeMillis();
        while ((inputLine = in.readLine()) != null) {
            String finalInputLine = inputLine;
            service.submit(() -> {
                if (czyPierwsza(Integer.valueOf(finalInputLine))) {
                    licznik.incrementAndGet();
                }
                latch.countDown();
            });
        }
        latch.await();
        in.close();
        long b = System.currentTimeMillis();
        System.out.println((b - a) + " milisekund");
        System.out.println(licznik.get());
        service.shutdown();
    }

    private static boolean czyPierwsza(int i) {
        int podzielnik = 3;
        if (i%2==0)return false;
        if (i > 3) {
            while (podzielnik < Math.sqrt(i)+1) {
                if (i % podzielnik == 0) {
                    return false;
                }
                podzielnik+=2;
            }
        }
        return true;
    }
}



