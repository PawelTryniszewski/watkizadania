package zad3;

import java.io.File;
import java.util.Objects;

public class Zadanie3 implements Runnable {

    private volatile String path = ".";
    private File folder=new File(".");

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public void run() {
        try {
            long b = Objects.requireNonNull(folder.listFiles()).length;
            while (true) {
                folder = new File(path);
                if (Objects.requireNonNull(folder.listFiles()).length < b) {
                    System.out.println("cos zostalo usuniete");
                    b = Objects.requireNonNull(folder.listFiles()).length;
                } else if (Objects.requireNonNull(folder.listFiles()).length > b) {
                    System.out.println("cos zostalo dodane");
                    b = Objects.requireNonNull(folder.listFiles()).length;
                }
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
